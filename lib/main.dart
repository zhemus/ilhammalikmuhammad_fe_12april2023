import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:malik_merkle/pages/detailPage.dart';
import 'package:malik_merkle/pages/login.dart';
import 'package:malik_merkle/pages/mainPage.dart';
import 'package:provider/provider.dart';

import 'components/brand.dart';
import 'pages/splash.dart';
import 'repository/theme_provider.dart';
import 'repository/user_provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    // registerNotification();
    // AwesomeNotifications().initialize(
    //   // set the icon to null if you want to use the default app icon
    //     null,
    //     [NotificationChannel(channelKey: 'basic_channel', channelName: 'Basic notifications', channelDescription: 'Notification channel for basic', defaultColor: ProjectTheme.applicationDefaultTheme.primaryColor, ledColor: Colors.white)]);
    // AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
    //   if (!isAllowed) {
    //     // Insert here your friendly dialog box before call the request method
    //     // This is very important to not harm the user experience
    //     AwesomeNotifications().requestPermissionToSendNotifications();
    //   }
    // });
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (context) => ThemeProvider()), ChangeNotifierProvider(create: (context) => UserProvider())],
      child: Consumer<ThemeProvider>(
        builder: (context, themeProvider, child) => MaterialApp(
          title: Brand.brandName,
          localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalWidgetsLocalizations.delegate, GlobalCupertinoLocalizations.delegate],
          theme: themeProvider.currentTheme,
          initialRoute: "/",
          onGenerateRoute: (settings) {
            print('build route for ${settings.name} :${settings.arguments}');
            var routes = <String, WidgetBuilder>{
              "/": (context) => Splash(),
              "/login": (context) => Login(),
              "/mainPage": (context) => MainPage(),
              "/detailPage": (context) => DetailPage(idUser: settings.arguments.toString()),
            };
            return MaterialPageRoute(settings: settings, builder: (ctx) => routes[settings.name]!(ctx));
          },
        ),
      ),
    );
  }
}