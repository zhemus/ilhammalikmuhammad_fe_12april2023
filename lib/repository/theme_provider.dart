import 'package:flutter/material.dart';

import '../utility/theme.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeData _currentThemeData = ProjectTheme.getCurrentTheme();

  ThemeData get currentTheme => _currentThemeData;

  void updateTheme(ThemeData newTheme) {
    _currentThemeData = newTheme;
    notifyListeners();
  }
}
