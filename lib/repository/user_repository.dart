import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';

class UserRepository {
  Future<void> saveUser(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = jsonEncode(user);
    await prefs.setString("activeUser", userString);
  }

  Future<void> deleteUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("activeUser", "");
  }

  Future<User?> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      String? userString = prefs.getString("activeUser");
      print(userString);
      User activeUser = User.fromJson(jsonDecode(userString!));
      return activeUser;
    } catch (e, stacktrace) {
      print(stacktrace);
      return null;
    }
  }

  Future<void> saveToken(String? token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("activeToken", token??'');
  }

  Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      String? token = prefs.getString("activeToken");
      return token;
    } catch (e, stacktrace) {
      print(stacktrace);
      return null;
    }
  }
}
