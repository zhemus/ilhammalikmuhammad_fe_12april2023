import 'package:malik_merkle/models/LoginResponse.dart';
import 'package:malik_merkle/models/getUsersResponse.dart';
import 'package:malik_merkle/models/user.dart';
import 'package:malik_merkle/models/user.dart';

import '../models/getUsersResponseNew.dart';
import '../network/api_caller.dart';
import '../network/api_service.dart';
import 'user_repository.dart';

class AuthRepository {
  ApiCaller _apiProvider = ApiCaller();
  UserRepository _userProvider = UserRepository();

  Future<LoginResponse> login(String username, String password) async {
    LoginResponse loginResponse = await ApiService.login(_apiProvider, username, password);
    _userProvider.saveToken(loginResponse.token);
    print(loginResponse.token);

    return loginResponse;
  }

  Future<GetUsersResponse> getUsers() async {
    GetUsersResponse response = await ApiService.getUsers(_apiProvider);
    print(response);

    return response;
  }

  Future<User> getUser(String idUser) async {
    User response = await ApiService.getUser(_apiProvider, idUser);
    print(response);

    return response;
  }

  Future<User> deleteUser(String idUser) async {
    User response = await ApiService.deleteUser(_apiProvider, idUser);
    print(response);

    return response;
  }
}
