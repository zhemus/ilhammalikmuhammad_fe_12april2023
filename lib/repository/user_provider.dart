import 'package:flutter/material.dart';

import '../models/user.dart';
import '../utility/theme.dart';

class UserProvider extends ChangeNotifier {
  ThemeData _currentThemeData = ProjectTheme.getCurrentTheme();
  ThemeData get currentTheme => _currentThemeData;
  late User _activeUser;
  User get activeUser => _activeUser;

  set activeUser(User activeUser) {
    _activeUser = activeUser;
    notifyListeners();
  }

  void updateTheme(ThemeData newTheme) {
    _currentThemeData = newTheme;
    notifyListeners();
  }
}
