/// token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXIiOiJtb3JfMjMxNCIsImlhdCI6MTY4MTMwNDc5Nn0.8BAjKM-VCyx_3DYsuLIS_wZWRHBodtuCI4lxHw0Dbmo"

class LoginResponse {
  LoginResponse({
    String? token,
  }) {
    _token = token;
  }

  LoginResponse.fromJson(dynamic json) {
    _token = json['token'];
  }

  String? _token;

  LoginResponse copyWith({
    String? token,
  }) =>
      LoginResponse(
        token: token ?? _token,
      );

  String? get token => _token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = _token;
    return map;
  }
}
