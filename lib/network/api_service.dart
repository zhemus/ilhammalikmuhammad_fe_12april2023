

import 'package:malik_merkle/models/getUsersResponse.dart';
import 'package:malik_merkle/models/user.dart';
import 'package:malik_merkle/network/api_caller.dart';

import '../models/LoginResponse.dart';

class ApiService {
  static Future<LoginResponse> login(ApiCaller apiProvider, String emailAddress, String password) async {
    final response = await apiProvider.post('auth/login', {'username': emailAddress, 'password': password});
    LoginResponse loginResponse = LoginResponse.fromJson(response);

    return loginResponse;
  }

  static Future<GetUsersResponse> getUsers(ApiCaller apiProvider) async {
    final response = await apiProvider.get('users', {});
    print('response-> $response');
    GetUsersResponse responseResult = GetUsersResponse.fromJson(response);

    return responseResult;
  }

  static Future<User> getUser(ApiCaller apiProvider, String idUser) async {
    final response = await apiProvider.get('users/$idUser', {});
    print('response-> $response');
    User responseResult = User.fromJson(response);

    return responseResult;
  }

  static Future<User> deleteUser(ApiCaller apiProvider, String idUser) async {
    final response = await apiProvider.delete('users/$idUser', {});
    print('response-> $response');
    User responseResult = User.fromJson(response);

    return responseResult;
  }

}
