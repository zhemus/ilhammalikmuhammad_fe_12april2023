import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:malik_merkle/components/loadings.dart';
import 'package:malik_merkle/components/text.dart';
import 'package:malik_merkle/models/user.dart';
import 'package:url_launcher/url_launcher.dart';

import '../components/buttons.dart';
import '../repository/AuthRepository.dart';
import '../utility/theme.dart';

class DetailPage extends StatefulWidget {
  final String idUser;

  const DetailPage({required this.idUser});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  bool _isLoading = false;
  late User user;

  Future<void> getUser() async {
    setState(() {
      _isLoading = true;
    });

    try {
      User response = await AuthRepository().getUser(widget.idUser);

      setState(() {
        user = response;
        _isLoading = false;
      });
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);

      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    print(widget.idUser);
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Scaffold(
      body: SafeArea(
        child: FullScreenLoading(message: 'Please wait...'),
      ),
    )
        : Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Align(
              alignment: Alignment.center,
              child: TextNormal(
                label: 'Edit',
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              AspectRatio(
                aspectRatio: 10 / 2,
                child: Container(
                  decoration: BoxDecoration(shape: BoxShape.circle, color: ProjectTheme.applicationDefaultTheme.disabledColor),
                  height: 32,
                  width: 32,
                  alignment: Alignment.center,
                  child: TextTitle(
                    label: '${(user.name!.firstname?.split(' ')[0])![0].toUpperCase()}${(user.name!.lastname?.split(' ')[0])![0].toUpperCase()}',
                    color: Colors.white,
                  ),
                  // child: const Icon(Icons.person, size: 28, color: Colors.white),
                ),
              ),
              const SizedBox(height: 32),
              Center(child: TextTitle(label: '${user.name!.firstname} ${user.name!.lastname}')),
              const SizedBox(height: 32),
              TextCaption(
                label: 'Email',
                color: ProjectTheme.applicationDefaultTheme.disabledColor,
              ),
              InkWell(
                onTap: () {
                  _makeEmail('mailto:${user.email}?subject=Hi, ${user.name!.firstname} ${user.name!.lastname}');
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [TextNormal(label: user.email!), const Icon(Icons.email_outlined)],
                ),
              ),
              const SizedBox(height: 16),
              TextCaption(
                label: 'Phone Number',
                color: ProjectTheme.applicationDefaultTheme.disabledColor,
              ),
              InkWell(
                onTap: () {
                  _makePhoneCall('tel:${user.phone}');
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [TextNormal(label: user.phone!), const Icon(Icons.phone)],
                ),
              ),
              const SizedBox(height: 16),
              TextCaption(
                label: 'Address',
                color: ProjectTheme.applicationDefaultTheme.disabledColor,
              ),
              TextNormal(label: '${user.address!.number} ${user.address!.street} ${user.address!.city} ${user.address!.zipcode}'),
              const Spacer(),
              BasicButton(
                  backgroundColor: Colors.redAccent,
                  label: 'Delete Account',
                  onPressed: () {
                    deleteAccount(user.id.toString());
                  })
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _makeEmail(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  void deleteAccount(String userId) async {
    setState(() {
      _isLoading = true;
    });

    try {
      User response = await AuthRepository().deleteUser(userId);

      setState(() {
        _isLoading = false;
      });

      Navigator.pop(context);
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);

      setState(() {
        _isLoading = false;
      });
    }
  }
}
