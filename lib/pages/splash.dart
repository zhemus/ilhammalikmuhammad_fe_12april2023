import 'package:flutter/material.dart';

import '../components/brand.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  void wait() async {
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, "/login");
    });
  }


  @override
  void initState() {
    super.initState();
    wait();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(70.0),
            child: Brand.iconLongWhite(),
          ),
        ),
      ),
    );
  }
}
