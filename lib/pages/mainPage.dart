import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:malik_merkle/components/loadings.dart';
import 'package:malik_merkle/models/getUsersResponse.dart';

import '../components/listItem.dart';
import '../repository/AuthRepository.dart';

class MainPage extends StatefulWidget {
  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool _isLoading = false;
  late GetUsersResponse users;

  Future<void> getUserList() async {
    setState(() {
      _isLoading = true;
    });

    try {
      GetUsersResponse response = await AuthRepository().getUsers();

      setState(() {
        users = response;
        _isLoading = false;
      });
      print('${users.users?.length}');
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);

      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    getUserList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Fluttertoast.showToast(msg: 'On Development');
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
      body: SafeArea(
        child: _isLoading && users.users == null
            ? FullScreenLoading(message: ('Please wait..'))
            : Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView.builder(
                  itemCount: users.users?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return UserItemCard(
                      onPressed: () {
                        Navigator.pushNamed(context, '/detailPage', arguments: users.users![index].id);
                      },
                      title: '${users.users![index].name!.firstname} ${users.users![index].name!.lastname}',
                      email: users.users![index].email,
                      phone: users.users![index].phone,
                    );
                  },
                ),
              ),
      ),
    );
  }
}
