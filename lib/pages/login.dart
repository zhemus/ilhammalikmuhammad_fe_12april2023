import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:malik_merkle/models/LoginResponse.dart';

import '../components/brand.dart';
import '../components/buttons.dart';
import '../components/text.dart';
import '../repository/AuthRepository.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _username = TextEditingController(text: 'mor_2314');
  final _password = TextEditingController(text: '83r5^_');

  bool _passwordVisible = false;

  String errorMessage = '';
  bool verifyError = false;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(padding: const EdgeInsets.all(16.0), child: Brand.iconLongWhite()),
              const SizedBox(height: 32),
              TextTitle(label: 'Hello!'),
              const SizedBox(height: 16),
              TextCaption(label: 'Please Login to continue', fontWeight: FontWeight.w700),
              BasicTextField(
                hintText: 'Username',
                controller: _username,
                keyboardType: TextInputType.text,
                function: (value) {
                  setState(() {
                    errorMessage = '';
                    verifyError = false;
                  });
                },
              ),
              const SizedBox(height: 16),
              PasswordTextField(
                hintText: 'Enter your password here',
                passwordVisible: _passwordVisible,
                controller: _password,
                onToggleVisibility: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
                function: (value) {
                  setState(() {
                    errorMessage = '';
                  });
                },
              ),
              const SizedBox(height: 8),
              verifyError ? TextCaption(label: errorMessage, color: const Color(0xFFD71440)) : TextCaption(label: '', color: const Color(0xFF00B4D8)),
              const Spacer(),
              SizedBox(
                width: double.infinity,
                child: _username.text.length > 2
                    ? _isLoading
                        ? BasicButton(
                            label: '',
                            child: const SpinKitCircle(
                              color: Colors.white,
                              size: 14,
                            ))
                        : BasicButton(
                            label: 'Next',
                            onPressed: () {
                              verify();
                              // print('buttom clicked');
                            })
                    : BasicButton(
                        label: 'Next',
                        backgroundColor: const Color(0xFFDEDEDE),
                      ),
              ),
              const SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }

  void verify() async {
    bool valid = true;
    String username = _username.text;
    String password = _password.text;

    if (username.isEmpty || password.isEmpty) {
      valid = false;
      setState(() {
        errorMessage = 'Please fill all fields';
        verifyError = true;
      });
    }

    if (valid) {
      if (!_isLoading) {
        setState(() {
          errorMessage = '';
          _isLoading = true;
        });

        try {
          LoginResponse loginResponse = await AuthRepository().login(_username.text, _password.text);

          if (loginResponse.token!.isNotEmpty) {
            setState(() {
              errorMessage = '';
              _isLoading = false;
            });

            Navigator.pushReplacementNamed(context, "/mainPage");
          }
        } catch (e, stacktrace) {
          print(e);
          print(stacktrace);

          setState(() {
            errorMessage = e.toString();
            verifyError = true;
            _isLoading = false;
          });
        }
      }
    }
  }
}
