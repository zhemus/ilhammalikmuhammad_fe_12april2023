import 'package:flutter/material.dart';

class SmallCard extends StatelessWidget {
  final Widget? child;
  final Color backgroundColor;
  final Color borderColor;
  final Function? onItemClick;

  SmallCard({this.child, required this.backgroundColor, required this.borderColor, this.onItemClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: borderColor, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: backgroundColor,
      ),
      child: Padding(padding: const EdgeInsets.all(8.0), child: child),
    );
  }
}
