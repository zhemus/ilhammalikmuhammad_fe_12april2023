import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../utility/theme.dart';
import 'card.dart';


class FullScreenLoading extends StatelessWidget {
  final String message;

  FullScreenLoading({required this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.black.withOpacity(0.7),
      child: SmallCard(
        backgroundColor: Colors.white,
        borderColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            children: [
              SpinKitRotatingCircle(
                color: ProjectTheme.applicationDefaultTheme.primaryColor,
                size: 50.0,
              ),
              SizedBox(
                height: 20,
              ),
              Text(message)
            ],
          ),
        ),
      ),
    );
  }
}
