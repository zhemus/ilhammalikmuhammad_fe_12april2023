import 'package:flutter/material.dart';

import '../utility/theme.dart';

class BasicButton extends StatelessWidget {
  final String label;
  final VoidCallback? onPressed;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final double? horizontal;
  final double? vertical;
  final Widget? child;

  BasicButton({required this.label, this.onPressed, this.backgroundColor, this.foregroundColor, this.horizontal, this.vertical, this.child});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: ButtonStyle(
        splashFactory: InkRipple.splashFactory,
        padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: vertical ?? 16, horizontal: horizontal ?? 40)),
        backgroundColor: MaterialStateProperty.all<Color>(backgroundColor ?? ProjectTheme.applicationDefaultTheme.colorScheme.secondary),
        foregroundColor: MaterialStateProperty.all<Color>(foregroundColor ?? Colors.white),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
      ),
      child: child ?? Text(
              label,
              style: TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
            ),
    );
  }
}

// class BasicButtonSmallText extends StatelessWidget {
//   final String label;
//   final Function onPressed;
//   final Color backgroundColor;
//   final Color foregroundColor;
//   final double horizontal;
//   final double vertical;
//   final Widget child;
//
//   BasicButtonSmallText({this.label, this.onPressed, this.backgroundColor, this.foregroundColor, this.horizontal, this.vertical, this.child});
//
//   @override
//   Widget build(BuildContext context) {
//     return TextButton(
//       onPressed: onPressed,
//       child: child == null
//           ? Text(
//         label,
//         style: TextStyle(fontSize: 12, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
//       )
//           : child,
//       style: ButtonStyle(
//         splashFactory: InkRipple.splashFactory,
//         padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.symmetric(vertical: vertical != null ? vertical : 16, horizontal: horizontal != null ? horizontal : 40)),
//         backgroundColor: MaterialStateProperty.all<Color>(backgroundColor != null ? backgroundColor : ProjectTheme.applicationDefaultTheme.colorScheme.secondary),
//         foregroundColor: MaterialStateProperty.all<Color>(foregroundColor != null ? foregroundColor : Colors.white),
//         shape: MaterialStateProperty.all<RoundedRectangleBorder>(
//           RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(30.0),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class HeaderButton extends StatelessWidget {
//   final String imagePath;
//   final Color color;
//   final Function onPressed;
//
//   HeaderButton({this.onPressed, this.imagePath, this.color});
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 48,
//       height: 48,
//       child: InkWell(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(vertical: 16.0),
//           child: Image.asset(
//             imagePath != null ? imagePath : "assets/ic_back.png",
//             height: 10,
//             color: color != null ? color : Color(0xFF323232),
//           ),
//         ),
//         onTap: onPressed,
//       ),
//     );
//   }
// }
