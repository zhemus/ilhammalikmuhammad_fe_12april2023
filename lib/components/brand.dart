import 'package:flutter/material.dart';

class Brand {
  static String brandName = "Maven Flutter Starter";

  static Widget textOnly() {
    return Text(
      brandName,
      style: TextStyle(
        fontSize: 16,
      ),
    );
  }

  static Widget iconLongWhite(){
    return Image.asset(
      "assets/ic_logo_full.png",
      width: 650,
      height: 100,
    );
  }

}
