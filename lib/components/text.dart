import 'package:flutter/material.dart';

class TextTitle extends StatelessWidget {
  final String label;
  final Color? color;

  TextTitle({required this.label, this.color});

  @override
  Widget build(BuildContext context) {
    return Text(label, style: TextStyle(color: color ?? const Color(0xFF323232), fontSize: 20, fontFamily: 'Poppins', fontWeight: FontWeight.w700));
  }
}

class TextDescription extends StatelessWidget {
  final String label;
  final TextAlign? alignment;

  TextDescription({required this.label, this.alignment});

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: const TextStyle(color: Color(0xFF323232), fontSize: 14, fontFamily: 'Poppins'),
      textAlign: alignment ?? TextAlign.start,
    );
  }
}

class TextCaption extends StatelessWidget {
  final String label;
  final Color? color;
  final FontWeight? fontWeight;
  final TextAlign? alignment;
  final TextOverflow? overflow;
  final double? size;

  TextCaption({required this.label, this.color, this.fontWeight, this.alignment, this.overflow, this.size});

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyle(fontSize: size ?? 12, color: color ?? const Color(0xFF323232), fontFamily: 'Poppins', fontWeight: fontWeight ?? FontWeight.normal),
      textAlign: alignment ?? TextAlign.start,
      overflow: overflow,
    );
  }
}

class TextNormal extends StatelessWidget {
  final String label;
  final Color? color;
  final FontWeight? fontWeight;

  TextNormal({required this.label, this.color, this.fontWeight});

  @override
  Widget build(BuildContext context) {
    return Text(label, style: TextStyle(fontSize: 14, color: color ?? Colors.black, fontFamily: 'Poppins', fontWeight: fontWeight ?? FontWeight.normal));
  }
}

class BasicTextField extends StatelessWidget {
  final TextEditingController controller;
  final String? labelText;
  final String? hintText;
  final TextInputType keyboardType;
  final bool isEnabled;
  final Function(String)? function;

  BasicTextField({required this.controller, this.labelText, this.hintText, this.keyboardType = TextInputType.text, this.isEnabled = true, this.function});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      controller: controller,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(enabled: isEnabled, labelText: labelText, hintText: hintText, hintStyle: const TextStyle(color: Color(0xFFC4C4C4), fontSize: 14, fontFamily: 'Poppins'), floatingLabelBehavior: FloatingLabelBehavior.always),
      style: const TextStyle(fontSize: 14, fontFamily: 'Poppins'),
      onChanged: function,
    );
  }
}

class PasswordTextField extends StatelessWidget {
  final TextEditingController controller;
  final bool passwordVisible;
  final VoidCallback onToggleVisibility;
  final String hintText;
  final Function(String)? function;

  PasswordTextField({required this.controller, required this.passwordVisible, required this.onToggleVisibility, required this.hintText, this.function});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      controller: controller,
      obscureText: !passwordVisible,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.never,
        hintText: hintText,
        hintStyle: const TextStyle(color: Color(0xFFC4C4C4), fontSize: 14, fontFamily: 'Poppins'),
        suffixIcon: IconButton(
          icon: Icon(
            passwordVisible ? Icons.visibility : Icons.visibility_off,
            color: const Color(0xFF323232),
          ),
          onPressed: onToggleVisibility,
        ),
      ),
      onChanged: function,
      style: const TextStyle(fontSize: 14, fontFamily: 'Poppins'),
    );
  }
}
