import 'package:flutter/material.dart';
import 'package:malik_merkle/components/text.dart';

import '../utility/theme.dart';

class UserItemCard extends StatelessWidget {
  final String? title;
  final String? email;
  final String? phone;
  final VoidCallback onPressed;

  const UserItemCard({Key? key, this.title, required this.onPressed, this.email, this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: ProjectTheme.applicationDefaultTheme.disabledColor),
          ),
        ),
        child: Row(
          children: [
            Container(
              decoration: BoxDecoration(shape: BoxShape.circle, color: ProjectTheme.applicationDefaultTheme.disabledColor),
              height: 32,
              width: 32,
              alignment: Alignment.center,
              child: TextNormal(
                label: title != null ? (title!.split(' ')[0])[0].toUpperCase()  : '',
                color: Colors.white,
              ),
              // child: const Icon(Icons.person, size: 28, color: Colors.white),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextNormal(label: title ?? ''),
                  const SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextCaption(
                        label: phone ?? '',
                      ),
                      TextCaption(
                        label: email ?? '',
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
